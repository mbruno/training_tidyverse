---
pagetitle: "Formation tidyverse cefe"
format: revealjs
author: Morgane BRUNO
---

{{< include slides/00-title.qmd >}}

{{< include slides/01-introduction.qmd >}}

{{< include slides/02-import-data.qmd >}}

{{< include slides/03-tidy-data.qmd >}}

{{< include slides/04-transform-data.qmd >}}

{{< include slides/06-visualize.qmd >}}