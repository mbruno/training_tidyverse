# Training R: Tidyverse

These are the slides from the 'Tidyverse' training session on November 23, 2023, organized by CEFE.

## How to build the presentation ?

1. Open `training_tidyverse.Rproj` in RStudio.

2. If it's not install already,install the `{renv}` package.

```
install.packages("renv")
```

3. Run `renv::restore()` in the R console to install all the required packages for this project.

4. Run `quarto render` in the RStudio Terminal

5. All done ! The presentation `training_r_tidyverse.html` and the worksheet `tp/tp.pdf` have been created. 

## Licence

All code is licensed under the [MIT Licence](LICENSE.md)
