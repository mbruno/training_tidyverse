---
pagetitle: "Tidy"
author: "Morgane Bruno"
---

# Data tidying {#tidying-1 background-color="#160f27ff" transition="zoom"}

![](images/tidy.png){fig-align="center"}

## Give your code some air with `|>` ? {#tidying-2}

<br>

:::{.center-text}
The point of the pipe is to help you write code in a way that is easier to read and understand.
:::

<br>

- `Verb(Subject,Complement)` replaced by `Subject |> Verb(Complement)`
- No need to name unimportant intermediate variables
- It easy to add steps anywhere in the sequence of operations

::: custom-citation
> Hadley Wickham (2023). Differences between the base R and magrittr pipes. <https://www.tidyverse.org/blog/2023/04/base-vs-magrittr-pipe/>
:::

## Give your code some air with `|>` ? {#tidying-3}

![](https://perso.ens-lyon.fr/lise.vaudor/Rfigures/Piping/piping_complete.jpg){fig-align="center"}

::: { .custom-citation}
Vaudor L (2018). Utiliser des pipes pour enchaîner des instructions. <https://perso.ens-lyon.fr/lise.vaudor/utiliser-des-pipes-pour-enchainer-des-instructions/>
:::


## What are Tibbles ? {#tidying-4}

:::{.center-text}
Tibbles *are* data frames, but ... Tibble vs. a classic data.frame :
:::

:::{.incremental}
- Never changes the type of the inputs (e.g. it never converts strings to factors!)
- Never creates row names
- Refined print method: shows only the first 10 rows, and all the columns that fit on screen.
:::

:::{.fragment fragment-index="5"}

```{r print}
palmerpenguins::penguins
```
:::

## What is tidy data ? {#tidying-5 auto-animate="true" auto-stretch="true"}

<br>

::: columns
::: {.column width="45%"}
<mark>**In tidy data:**</mark>

::: {.fragment fragment-index="2" style="margin-right: 0"}
1.  Each <mark style="background-color:#9BFF39">**variable**</mark> forms a <mark style="background-color:#9BFF39">**column**</mark>.
:::

::: {.fragment fragment-index="3" style="margin-right: 0"}
2.  Each <mark style="background-color:#15FDD8">**observation**</mark> forms a <mark style="background-color:#15FDD8">**row**</mark>.
:::

::: {.fragment fragment-index="4" style="margin-right: 0"}
3.  Each <mark style="background-color:#FF289E">**cell**</mark> is a <mark style="background-color:#FF289E">**single value**</mark>.

::: {.callout-note}
*What is a variable and an observation may depend one your immediate goal*
:::

:::
:::

::: {.column width="55%"}
::: {.r-stack style="margin-right: 0"}
![](images/tidy1.png){.fragment fragment-index="2"}

![](images/tidy2.png){.fragment fragment-index="3"}

![](images/tidy3.png){.fragment fragment-index="4"}
:::
:::
:::

::: custom-citation
> Wickham, H. (2014). Tidy Data. Journal of Statistical Software 59 (10).<br /> DOI: [10.18637/JSS.V059.I10](https://www.jstatsoft.org/article/view/v059i10)
:::

## Quiz 1: Is this dataset tidy ? {#tidying-6 auto-animate="true" auto-stretch="true"}

<br>

```{r not-tidy-1}
#| echo: false

penguins_no_tidy <- palmerpenguins::penguins |>
  count(species, island, name = "count") |>
  tidyr::pivot_wider(
    names_from = island,
    values_from = count
  )

penguins_no_tidy |>
  kableExtra::kable(booktabs = TRUE) |>
  kableExtra::kable_material(c("striped", "hover"))
```

<br>


::: {.fragment .center-text}
**What are the variables in cases ?**
:::

::: {.fragment}
- Species
- Island
- Count
:::

::: {.fragment .center-text}
~~Each <mark style="background-color:#9BFF39">**variable**</mark> forms a <mark style="background-color:#9BFF39">**column**</mark>.~~
:::

## Quiz 2: Is this dataset tidy ? {#tidying-7 auto-animate="true" auto-stretch="true"}

```{r not-tidy-2}
#| echo: false

penguins_no_tidy_2 <- penguins_data |>
  dplyr::select(`Individual ID`, `Culmen Length (mm)`, `Culmen Depth (mm)`) |>
  tidyr::pivot_longer(
    cols = 2:3,
    names_to = "Measurement",
    values_to = "Values"
  ) |>
  tidyr::separate(
    col = 'Measurement',
    into = c('Body Part', 'Measurement', 'Unit'),
    sep = " "
  ) |>
  dplyr::select(-Unit) |>
  tidyr::drop_na() |>
  head(n = 6)

penguins_no_tidy_2 |>
  kableExtra::kable(booktabs = TRUE) |>
  kableExtra::kable_material(c("striped", "hover"))
```

::: {.fragment .center-text}
**What are the variables in culmen ?**
:::

::: {.fragment}
- Depth
- Length
:::

::: {.fragment .center-text}
~~Each <mark style="background-color:#15FDD8">**observation**</mark> forms a <mark style="background-color:#15FDD8">**row**</mark>.~~
:::

## Pivoting {#tidying-8}

<br>

::: columns
::: {.column width="50%"}
- `tidyr::pivot_longer()`

![](https://thinkr.fr/wp-content/uploads/wide_long.png)
:::

::: {.column width="50%"}
- `tidyr::pivot_wider()`

![](https://thinkr.fr/wp-content/uploads/long_wide.png)
:::
:::

::: {.custom-citation}
> Salette E. (2020). Manipuler ses données avec tidyr ou comment éviter la surchauffe cérébrale. ThinkR. <https://thinkr.fr/manipuler-ses-donnees-avec-tidyr-ou-tout-ce-que-vous-voulez-savoir-sur-le-pivot/>
:::

## Pivoting: pivot_longer() {#tidying-9}

::: columns
::: {.column width="60%"}

<br>

```{r not-tidy-3}
#| echo: false

penguins_no_tidy |>
  kableExtra::kable(align = "c") |>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  )
```

<br>

```{r fun-pivot-l}
#| eval: false

penguins_no_tidy |>     # tibble to pivot
  tidyr::pivot_longer(
    cols = 2:4,          # indexes columns to collapse
    names_to = "island", # name of the new key column
    values_to = "count"  # name of the new value column
)
```
:::

::: {.column width="40%"}
```{r penguins-longer}
#| echo: false

penguins_longer <- penguins_no_tidy |>
  tidyr::pivot_longer(cols = -species, names_to = "island", values_to = "count")

penguins_longer |>
  kableExtra::kable(align = "c")|>
  kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  ) |>
  kableExtra::add_header_above(
    header = c(" " = 1, "key" = 1, "value" = 1),
    bold = TRUE,
    align = "c",
    color = "#FF5733"
  )
```
:::
:::

## Pivoting: pivot_wider() {#tidying-10}

::: columns
::: {.column width="60%"}
```{r penguins-wider}
#| echo: false

penguins_no_tidy_2 |>
  kableExtra::kable(align = "c")|>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  ) |>
  kableExtra::add_header_above(
    header = c(" " = 2, "key" = 1, "value" = 1),
    bold = TRUE,
    align = "c",
    color = "#FF5733"
  )
```
:::

::: {.column width="40%"}

<br>

```{r not-tidy-4}
#| echo: false

penguins_longer_2 <- penguins_no_tidy_2 |>
  tidyr::pivot_wider(
    names_from = "Measurement",
    values_from = "Values"
  ) 
penguins_longer_2|>
  kableExtra::kable(align = "c")|>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  )
```

<br>

```{r fun-pivot-w}
#| eval: false

penguins_no_tidy_2 |>            # tibble to pivot
  tidyr::pivot_wider(
    names_from = "Measurement",  # column to use for key
    values_from = "Values"       # column to use for value
  )
```
:::
:::

## Handling Missing Values: drop_na() {#tidying-11}

::: columns
::: {.column width="40%"}
```{r penguins-with-na-1}
#| echo: false

penguins_longer |>
  kableExtra::kable(align = "c")|>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  ) |>
  kableExtra::column_spec(
    column = 3,
    background = c(rep("white", 3), "#FF5733", "white", rep("#FF5733", 3), "white")
  )
```
:::

::: {.column width="60%"}

<br>

```{r fun-drop-na}
#| eval: false

penguins_longer |> 
  tidyr::drop_na()
```

<br>

```{r penguins-longer-drop-na}
#| echo: false

penguins_longer |>
  tidyr::drop_na() |>
  kableExtra::kable(align = "c")|>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  )
```
:::
:::

## Handling Missing Values: replace_na() {#tidying-12}

::: columns
::: {.column width="40%"}
```{r penguins-with-na-2}
#| echo: false

penguins_longer |>
  kableExtra::kable(align = "c")|>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  ) |>
  kableExtra::column_spec(
    column = 3,
    background = c(rep("white", 3), "#FF5733", "white", rep("#FF5733", 3), "white")
  )
```
:::

::: {.column width="60%"}

```{r fun-replace-na}
#| eval: false

penguins_longer |> 
  tidyr::replace_na(list(count = 0))
```

```{r penguins-longer-replace-na}
#| echo: false

penguins_longer |>
  tidyr::replace_na(list(count = 0)) |>
  kableExtra::kable(align = "c")|>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = F
  )
```
:::
:::

## Separate and unite {#tidying-13}

<br>

![](images/separate_unite.png){fig-align="center"}

## Separate example {#tidying-14}

::: columns
::: {.column width="50%"}
```{r seperate-data}
#| echo: false
penguins_data |>
  dplyr::select(studyName, `Date Egg`) |>
  head(n = 4) |>
  kableExtra::kable(align = "c") |>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = T
  )
```
:::

:::{.column width="50%"}
```{r fun-seperate}
#| eval: false
#| code-line-numbers: "3-7"

penguins_data |>
  dplyr::select(studyName, `Date Egg`) |>
  tidyr::separate(
    col = 'Date Egg',
    into = c('Year', 'Month', 'Day'),
    sep = '-'
  )
```
:::
:::

<br>

```{r separate-example}
#| echo: false
penguins_data |>
  dplyr::select(studyName, `Date Egg`) |>
  head(n = 3) |>
  tidyr::separate(
    col = 'Date Egg',
    into = c('Year', 'Month', 'Day'),
    sep = '-'
  ) |>
  kableExtra::kable(align = "c") |>
   kableExtra::kable_styling(
     bootstrap_options = "striped", 
     full_width = T
  )
```
